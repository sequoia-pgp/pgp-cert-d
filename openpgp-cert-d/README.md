# Shared OpenPGP Certificate Directory

This crate implements a generic [OpenPGP certificate store] that can
be shared between implementations.  It also defines a way to root
trust, and a way to associate pet names with certificates.
Sharing certificates and trust decisions increases security by
enabling more applications to take advantage of OpenPGP.  It also
improves privacy by reducing the required certificate discoveries
that go out to the network.

[OpenPGP certificate store]: https://datatracker.ietf.org/doc/draft-nwjw-openpgp-cert-d/
